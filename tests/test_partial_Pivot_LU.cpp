#include <fstream>
#include <iostream>
#include <Eigen/Dense>
#include <ctime>
#include <cmath>
#include <iomanip>
#include "partial_Piv_LU.hpp"
#include "get_QR.hpp"
#include "get_Compressed_Low_Rank.hpp"

int main(int argc, char* argv[]) {
	srand(time(NULL));
    // int m           =       atoi(argv[1]);	//      Number of rows.
    // int n           =       atoi(argv[2]);	//      Number of columns.
	//     int p           =       atoi(argv[3]);	//      Rank of the matrix.
	// int tol			=		atoi(argv[4]);	//		-Log10 of tolerance

	int	p	=	10;
	int tol	=	16;

	int nmax=	600;
	double time_PPLU[nmax];
	double time_Unitary[nmax];
	clock_t start, end;
	int rank;
	double CPS	=	CLOCKS_PER_SEC;

	Eigen::MatrixXd Q,R;


	int nprec	=	4;
	for (int j=0;j<nmax;++j) {
		int n	=	p+j*p;
		Eigen::MatrixXd A;
		int	m	=	n;

		Eigen::MatrixXd Qx, Qz, R;
		// Eigen::MatrixXd X       =       Eigen::MatrixXd::Random(m,p);
		// get_QR(X, Qx, R);
		Qx						=	Eigen::MatrixXd::Random(m,p);
		Eigen::VectorXd d(p);
		// Eigen::MatrixXd Z       =       Eigen::MatrixXd::Random(p,n);
		// get_QR(Z, Qz, R);
		Qz						=	Eigen::MatrixXd::Random(n,p);

		for (int l=0; l<p; ++l) {
			d(l)	=	pow(10.0,1.0-16.0*l/p);
			// d(l)	=	1.0;
		}

		A	=		Qx*d.asDiagonal()*Qz.transpose();

		// start	=	clock();
		// get_QR(A,Q,R);
		// end		=	clock();
		// std::cout << "Time taken for QR: " << (end-start)/CPS <<"\n";
		// Eigen::VectorXd x(m), y(n);
		// for (int k=0;k<m;++k) {
		// 	x(k)	=	double(k)/m;
		// }
		// for (int k=0;k<n;++k) {
		// 	y(k)	=	1.0+double(k)/n;
		// }
		// for (int j=0; j<m; ++j) {
		// 	for (int k=0; k<n; ++k) {
		// 		A(j,k)	=	exp(-(x(j)-y(k))*(x(j)-y(k)));
		// 	}
		// }

	    double tolerance	=       pow(10,-tol);
		Eigen::MatrixXd U, V;
		start	=	clock();
		partial_Piv_LU(A, tolerance, U, V, rank);
		end		=	clock();
		// Eigen::MatrixXd error	=	A-U*V.transpose();
		time_PPLU[j]	=	(end-start)/CPS;
		std::cout << n << "\t";
		std::cout << "Pre: \t" << std::scientific << std::setprecision(nprec) << time_PPLU[j] << "\n";
		// std::cout << "Rank is: " << rank << "\n";
		// std::cout << "2 norm error is: " << error.norm() << "\n";
		// std::cout << "relative norm error is: " << error.norm()/A.norm() << "\n";
		// std::cout << "\t Max error is: " << std::setprecision(nprec) << error.cwiseAbs().maxCoeff() << "\t";

		Eigen::MatrixXd Q1,Q2,K;
		start	=	clock();
		get_Compressed_Low_Rank(U, V, Q1, K, Q2);
		end		=	clock();
		// error	=	A-Q1*K*Q2.transpose();
		time_Unitary[j]	=	(end-start)/CPS;
		std::cout << "Post: \t" << std::scientific << std::setprecision(nprec) << time_Unitary[j] << "\n";
		// std::cout << "Rank is: " << rank << "\n";
		// std::cout << "2 norm error is: " << error.norm() << "\n";
		// std::cout << "relative norm error is: " << error.norm()/A.norm() << "\n";
		// std::cout << "\t Max error is: " << std::setprecision(nprec) << error.cwiseAbs().maxCoeff() << "\n";
	}

	std::ofstream myFile;
	myFile.open("partial_Piv_LU.tex");
	myFile << "\\documentclass[landscape]{article}\n";
	myFile << "\\usepackage{tikz,pgfplots}\n";
	myFile << "\\begin{document}\n";
	myFile << "\\begin{figure}\n";
	myFile << "\\begin{tikzpicture}\n";
	myFile << "\\begin{axis}[xlabel=System size, ylabel=Time in seconds]\n";
	myFile << "\\addplot[blue] coordinates{";
	for (int j=0; j<nmax; ++j) {
		myFile << "(" << p+j*p << "," << time_PPLU[j] << ")";
	}
	myFile << "};\n\\addplot[red] coordinates{";
	for (int j=0; j<nmax; ++j) {
		myFile << "(" << p+j*p << "," << time_Unitary[j] << ")";
	}
	myFile << "};\n";
	myFile << "\\end{axis}\n";
	myFile << "\\end{tikzpicture}\n";
	myFile << "\\end{figure}\n";
	myFile << "\\end{document}";
	myFile.close();
}