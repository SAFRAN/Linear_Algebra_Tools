#include<iostream>
#include<set>
#include<vector>
#include<Eigen/Dense>

int main() {
	srand(time(NULL));
	int m	=	10;
	Eigen::VectorXd A	=	Eigen::VectorXd::Random(m);
	int r;
	double s	=	A.maxCoeff(&r);
	std::cout << "Maximum element is: " << s << "\n";
	std::cout << "Index of maximum element is: " << r << "\n";
	std::cout << A << "\n";
	std::set<int> v;
	v.insert(10);
	v.insert(20);
	std::set<int>::iterator it;
	for (it=v.begin(); it!=v.end(); ++it) {
		std::cout << *it << "\t";
	}
	std::cout << *(--v.end()) << "\n";
	std::vector<int> w;
	w.push_back(10);
	w.push_back(20);
	std::cout << w.back() << "\n";
	std::cout << v.count(20) << "\n";
	std::cout << v.size() << "\n";
	std::cout << rand()%20 << "\n";
	std::cout << v.size() << "\n";
}