//
//  get_Compressed_Low_Rank.hpp
//
//
//  Created by Sivaram Ambikasaran on 2/25/14.
//
//

#ifndef __get_Compressed_Low_Rank_hpp__
#define __get_Compressed_Low_Rank_hpp__

#include "get_QR.hpp"
#include "get_SVD.hpp"
#include <Eigen/Dense>


/*!
 Given a matrix in low-rank form converts it to PSQ^T, i.e., XY^T = Q_X S Q_Y^T, where P and Q are unitary matrices.
*/
void get_Compressed_Low_Rank(const Eigen::MatrixXd X, const Eigen::MatrixXd Y, Eigen::MatrixXd& Q_X, Eigen::MatrixXd& S, Eigen::MatrixXd& Q_Y) {
		//		Obtains QR of the matrix X.
		Eigen::MatrixXd R_X;
		get_QR(X, Q_X, R_X);
		//		Obtains QR of the matrix Y.
		Eigen::MatrixXd R_Y;
		get_QR(Y, Q_Y, R_Y);
		//		Obtains the matrix S
		S	=	R_X*R_Y.transpose();
}

/*!
 Given a matrix in low-rank form converts it to PSQ^T, i.e., XZY^T = Q_X S Q_Y^T, where P and Q are unitary matrices.
*/
void get_Compressed_Low_Rank(const Eigen::MatrixXd X, const Eigen::MatrixXd Z, const Eigen::MatrixXd Y, Eigen::MatrixXd& Q_X, Eigen::MatrixXd& S, Eigen::MatrixXd& Q_Y) {
		//		Obtains QR of the matrix X.
		Eigen::MatrixXd R_X;
		get_QR(X, Q_X, R_X);
		//		Obtains QR of the matrix Y.
		Eigen::MatrixXd R_Y;
		get_QR(Y, Q_Y, R_Y);
		//		Obtains the matrix S
		S	=	R_X*Z*R_Y.transpose();
}

/*!
 Given a matrix in low-rank form obtains its SVD, i.e., XY^T = USV^T, where the matrices are stored using Eigen's MatrixXd.
 */
void get_Compressed_Low_Rank(const Eigen::MatrixXd X, const Eigen::MatrixXd Y, const double tolerance, Eigen::MatrixXd& U, Eigen::VectorXd& S, Eigen::MatrixXd& V, int& r) {
        //      Obtains compressed form, i.e., Q_X S Q_Y^T.
        Eigen::MatrixXd Q_X, Si, Q_Y;
		get_Compressed_Low_Rank(X, Y, Q_X, Si, Q_Y);

        //      Obtains SVD of Si.
        get_SVD(Si, tolerance, U, S, V, r);    //      Si = U S V^T.

        //      Obtains the row and column basis U and V.
        U       =       Q_X*U;
        V       =       Q_Y*V;
}

/*!
 Given a matrix in low-rank form obtains its SVD, i.e., XZY^T = USV^T, where the matrices are stored using Eigen's MatrixXd.
 */
void get_Compressed_Low_Rank(const Eigen::MatrixXd X, const Eigen::MatrixXd Z, const Eigen::MatrixXd Y, const double tolerance, Eigen::MatrixXd& U, Eigen::VectorXd& S, Eigen::MatrixXd& V, int& r) {
        //      Obtains compressed form, i.e., Q_X S Q_Y^T.
        Eigen::MatrixXd Q_X, Si, Q_Y;
		get_Compressed_Low_Rank(X, Z, Y, Q_X, Si, Q_Y);

        //      Obtains SVD of Si.
        get_SVD(Si, tolerance, U, S, V, r);  //      Si = U S V^T.

        //      Obtains the row and column basis U and V.
        U       =       Q_X*U;
        V       =       Q_Y*V;
}
#endif /* defined(__get_Compressed_Low_Rank__) */