#ifndef __get_Low_Rank_hpp__
#define __get_Low_Rank_hpp__

#include <Eigen/Dense>
#include <vector>
#include <set>
#include <algorithm>
#include <cmath>

void get_Low_Rank(const Eigen::MatrixXd A, const double tolerance, Eigen::MatrixXd& U, Eigen::MatrixXd& V, int& rank) {
	int m	=	A.rows();
	int n	=	A.cols();
	std::vector<int> rowIndex;
	std::vector<int> colIndex;
	std::set<int> remainingRowIndex;
	for (int k=0; k<m; ++k) {
		remainingRowIndex.insert(k);
	}
	std::set<int> remainingColIndex;
	for (int k=0; k<n; ++k) {
		remainingColIndex.insert(k);
	}
	std::vector<Eigen::VectorXd> rowBasis;
	std::vector<Eigen::VectorXd> colBasis;

	//	First select the first row
	int index	=	0;
	rowIndex.push_back(index);
	remainingRowIndex.erase(index);

	Eigen::VectorXd row, col;
	double maxElement;

	//	Select the first row
	row	=	A.row(index);

	//	Find the maximum element
	maxElement	=	row.cwiseAbs().maxCoeff(&index);
	rowBasis.push_back(row/maxElement);

	//	Select the column corresponding to the maximum element
	colIndex.push_back(index);
	remainingColIndex.erase(index);

	col	=	A.col(index);
	colBasis.push_back(col);

	rank	=	1;
	double norm_Of_Approximation	=	row.squaredNorm()*col.squaredNorm();
	std::set<int>::iterator it;
	int p		=	std::min(m,n);
	int count	=	0;
	int maxTries=	10;
	// int maxCount=	10;
	do {
		maxElement	=	col.cwiseAbs().maxCoeff(&index);
		// it			=	remainingRowIndex.begin();
		while (remainingRowIndex.count(index) == 0 && (maxElement <tolerance || count <maxTries)) {
			//	&& maxElement < tolerance && it!=remainingRowIndex.end()) {
			//	If the index has already been accounted for, then the new index is the first index in the reaminingRowIndex array.
			// index		=	*it;
			index		=	rand()%col.size();
			maxElement	=	fabs(col(index));
			++count;
			// ++it;
		}
		count	=	0;
		rowIndex.push_back(index);
		remainingRowIndex.erase(index);

		row	=	A.row(index);
		for (int l=0; l<rank; ++l) {
			row	=	row-colBasis[l](index)*rowBasis[l];
		}

		// std::cout << 2 << "\n";
		maxElement	=	row.cwiseAbs().maxCoeff(&index);
		// it			=	remainingColIndex.begin();
		while (remainingColIndex.count(index) == 0 && (maxElement <tolerance || count <maxTries)) {
			//	&& maxElement < tolerance && it!=remainingRowIndex.end()) {
			//	If the index has already been accounted for, then the new index is the first index in the reaminingRowIndex array.
			// index		=	*it;
			index		=	rand()%row.size();
			maxElement	=	fabs(row(index));
			++count;
			// ++it;
		}
		count	=	0;
		rowBasis.push_back(row/maxElement);

		colIndex.push_back(index);
		remainingColIndex.erase(index);

		col	=	A.col(index);
		for (int l=0; l<rank; ++l) {
			col	=	col-rowBasis[l](index)*colBasis[l];
		}
		colBasis.push_back(col);
		
		double tempsquaredNorm	=	row.squaredNorm()*col.squaredNorm();
		for (int l=0; l<rank; ++l) {
			tempsquaredNorm+=2*fabs(rowBasis[l].dot(row))*fabs(colBasis[l].dot(col));
		}
		norm_Of_Approximation+=tempsquaredNorm;
		++rank;
		if (row.norm()*col.norm() < tolerance && row.norm()*col.norm() < tolerance*sqrt(norm_Of_Approximation)) {
			++count;
		}
		// std::cout << rank << "\n";
	} while(rank<p && count<0.1*rank);
	U	=	Eigen::MatrixXd(m,rank);
	V	=	Eigen::MatrixXd(rank,n);
	for (int j=0; j<rank; ++j) {
		U.col(j)	=	colBasis[j];
		V.row(j)	=	rowBasis[j];
	}
}

#endif