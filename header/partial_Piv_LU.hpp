#ifndef __partial_Piv_LU_hpp__
#define __partial_Piv_LU_hpp__

#include <Eigen/Dense>
#include <vector>
#include <set>
#include <algorithm>
#include <cmath>

unsigned max_Abs_Vector(const Eigen::VectorXd& v, const std::set<int>& allowed_Indices, double& max) {
	std::set<int>::iterator it	=	allowed_Indices.begin();
	unsigned index	=	*allowed_Indices.begin();
	max				=	v(index);
	for (it=allowed_Indices.begin(); it!=allowed_Indices.end(); ++it) {
		if(fabs(v(*it))>fabs(max)) {
			index	=	*it;
			max		=	v(index);
		}
	}
	return index;
}

// unsigned max_Abs_Vector(const Eigen::VectorXd& v, const std::vector<int>& not_Allowed_Indices, double& max) {
// 		unsigned n      =   v.size();
// 		max             =   v(0);
// 		unsigned index  =   0;
// 		for(unsigned j=0; j<n; ++j){
// 			if(find(not_Allowed_Indices.begin(),not_Allowed_Indices.end(),j)==not_Allowed_Indices.end()) {
// 				if(fabs(v(j))>fabs(max)){
// 					max     =   v(j);
// 					index   =   j;
// 				}
// 			}
// 		}
// 		return index;
// }

void partial_Piv_LU(const Eigen::MatrixXd A, const double tolerance, Eigen::MatrixXd& U, Eigen::MatrixXd& V, int& computed_Rank) {

	/********************************/
	/*	PURPOSE OF EXISTENCE	*/
	/********************************/

	/*!
         Obtains the low-rank decomposition of the matrix to a desired tolerance using the partial pivoting LU algorithm, i.e., given a sub-matrix 'A' and tolerance 'epsilon', computes matrices 'U' and 'V' such that ||A-UV'||_F < epsilon. The norm is Frobenius norm.
	*/

	/************************/
	/*	INPUTS          */
	/************************/

	///	start_Row	-	Starting row of the sub-matrix.
	///	start_Col	-	Starting column of the sub-matrix.
	///	n_Rows		-	Number of rows of the sub-matrix.
	///	n_Cols		-	Number of columns of the sub-matrix.
	///	tolerance	-	Tolerance of low-rank approximation.

	/************************/
	/*	OUTPUTS		*/
	/************************/

	///	computed_Rank	-	Rank obtained for the given tolerance.
	///	U		-	Matrix forming the column basis.
	///	V		-	Matrix forming the row basis.

	/// If the matrix is small enough, do not do anything
	int n_Rows	=	A.rows();
	int n_Cols	=	A.cols();
		int tolerable_Rank =   5;
		if (n_Cols <= tolerable_Rank){
			U				=	A;
			V               =   Eigen::MatrixXd::Identity(n_Cols, n_Cols);
			computed_Rank   =   n_Cols;
			return;
		}
		else if (n_Rows <= tolerable_Rank){
			U               =   Eigen::MatrixXd::Identity(n_Rows, n_Rows);
			V				=	A;
			computed_Rank   =   n_Rows;
			return;
		}

		std::vector<int> rowIndex;		///	This stores the row indices, which have already been used.
		std::vector<int> colIndex;		///	This stores the column indices, which have already been used.
		std::set<int> remainingRowIndex;/// Remaining row indicies
		std::set<int> remainingColIndex;/// Remaining row indicies
		std::vector<Eigen::VectorXd> u;	///	Stores the column basis.
		std::vector<Eigen::VectorXd> v;	///	Stores the row basis.

		for (int k=0; k<n_Rows; ++k) {
			remainingRowIndex.insert(k);
		}
		for (int k=0; k<n_Cols; ++k) {
			remainingColIndex.insert(k);
		}

		srand (time(NULL));
		double max, Gamma, unused_max;

		/*  INITIALIZATION  */

		/// Initialize the matrix norm and the the first row index
		double matrix_Norm  =   0;
		rowIndex.push_back(0);
		remainingRowIndex.erase(0);

		int pivot;

		computed_Rank   =   0;

		Eigen::VectorXd a, row, col;

		double row_Squared_Norm, row_Norm, col_Squared_Norm, col_Norm;

		/// Repeat till the desired tolerance is obtained
		do {
			/// Generation of the row
			/// Row of the residuum and the pivot column
			row =   A.row(rowIndex.back());
			for (int l=0; l<computed_Rank; ++l) {
				row =   row-u[l](rowIndex.back())*v[l];
			}

			pivot   =   max_Abs_Vector(row, remainingColIndex, max);

			int max_tries  =   10;
			int count      =   0;

			/// This randomization is needed if in the middle of the algorithm the row happens to be exactly the linear combination of the previous rows upto some tolerance.
			while (fabs(max)<tolerance && count < max_tries) {
				rowIndex.pop_back();
				int new_rowIndex	=	*remainingRowIndex.begin();
				rowIndex.push_back(new_rowIndex);
				remainingRowIndex.erase(new_rowIndex);

				/// Generation of the row
				a	=	A.row(new_rowIndex);

				/// Row of the residuum and the pivot column
				row =   a;
				for (int l=0; l<computed_Rank; ++l) {
					row =   row-u[l](rowIndex.back())*v[l];
				}
				pivot   =   max_Abs_Vector(row, remainingColIndex, max);
				++count;
			}

			if (count == max_tries) break;

			count = 0;

			colIndex.push_back(pivot);
			remainingColIndex.erase(pivot);

			/// Normalizing constant
			Gamma   =   1.0/max;

			/// Generation of the column
			a	=	A.col(colIndex.back());

			/// Column of the residuum and the pivot row
			col =   a;
			for (int l=0; l<computed_Rank; ++l) {
				col =   col-v[l](colIndex.back())*u[l];
			}
			pivot   =   max_Abs_Vector(col, remainingRowIndex, unused_max);

			/// This randomization is needed if in the middle of the algorithm the columns happens to be exactly the linear combination of the previous columns.
			while (fabs(max)<tolerance && count < max_tries) {
				colIndex.pop_back();
				int new_colIndex	=	*remainingColIndex.begin();
				colIndex.push_back(new_colIndex);
				remainingColIndex.erase(new_colIndex);

				/// Generation of the column
				a	=	A.col(new_colIndex);

				/// Column of the residuum and the pivot row
				col =   a;
				for (int l=0; l<computed_Rank; ++l) {
					col =   col-u[l](colIndex.back())*v[l];
				}
				pivot   =   max_Abs_Vector(col, remainingRowIndex, unused_max);
				++count;
			}

			if (count == max_tries) break;

			count = 0;

			rowIndex.push_back(pivot);
			remainingRowIndex.erase(pivot);

			/// New vectors
			u.push_back(Gamma*col);
			v.push_back(row);

			/// New approximation of matrix norm
			row_Squared_Norm    =   row.squaredNorm();
			row_Norm            =   sqrt(row_Squared_Norm);

			col_Squared_Norm    =   col.squaredNorm();
			col_Norm            =   sqrt(col_Squared_Norm);

			matrix_Norm         =   matrix_Norm +   Gamma*Gamma*row_Squared_Norm*col_Squared_Norm;

			for (int j=0; j<computed_Rank; ++j) {
				matrix_Norm     =   matrix_Norm +   2.0*(u[j].dot(u.back()))*(v[j].dot(v.back()));
			}
			++computed_Rank;
		} while (computed_Rank*row_Norm*col_Norm > fabs(max)*tolerance*matrix_Norm && computed_Rank < fmin(n_Rows, n_Cols)-1);

		/// If the computed_Rank is close to full-rank then return the trivial full-rank decomposition
		if (computed_Rank>=fmin(n_Rows, n_Cols)-1) {
			if (n_Rows < n_Cols) {
				U   =   Eigen::MatrixXd::Identity(n_Rows,n_Rows);
				V	=	A.transpose();
				computed_Rank   =   n_Rows;
			}
			else {
				U	=	A;
				V   =   Eigen::MatrixXd::Identity(n_Cols,n_Cols);
				computed_Rank   =   n_Cols;
			}
		}
		else {
			U   =   Eigen::MatrixXd(n_Rows,computed_Rank);
			V   =   Eigen::MatrixXd(n_Cols,computed_Rank);
			for (int j=0; j<computed_Rank; ++j) {
				U.col(j)    =   u[j];
				V.col(j)    =   v[j];
			}			
		}
		return;
	}
#endif